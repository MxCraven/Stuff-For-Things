#!/bin/sh

DSPATH=$(dirname $0)
case "$2" in
	old)
		PATTERN='all-time fastest lap record with (.*)\n'
		;;
	cts)
		PATTERN='//CTS RECORD SET (.*)\n'
		;;
	ctf)
		PATTERN='//CTF RECORD SET (.*)\n'
		;;
	new|race|rc|*)
		PATTERN='//RA?CE? RECORD SET (.*)\n'
		;;
esac

d=$1
i=0
#All PBs on demo \/
$DSPATH/demoseek.pl grep "$d" "$PATTERN" | sort -rn -k 4 | while IFS=" " read -r timecode result; do
#Only final PB time \/
#$DSPATH/demoseek.pl grep "$d" "$PATTERN" | sort -rn -k 4 | tail -n 1 | while IFS=" " read -r timecode result; do
	timecode=${timecode%:}
	result=${result#\"}
	result=${result%\"}
	result=${result%% *}

	echo "Possible record found at $timecode: $result, extracting..."

	minutes=${result%%:*}
	result=${result#*:}
	seconds=${result%%.*}
	result=${result#*.}
	tenths=$result

	timecode_start=`echo "$timecode - $minutes*60 - $seconds - $tenths*0.1 - 2" | bc -l`
	timecode_end=`echo "$timecode + 2" | bc -l`
	i=$(($i + 1))
	$DSPATH/demoseek.pl seek "$d" "playback-$i.dem" "$timecode_start" "$timecode_end"
	$DSPATH/demoseek.pl seek "$d" "capture-$i.dem" "$timecode_start" "$timecode_end" --capture
done
