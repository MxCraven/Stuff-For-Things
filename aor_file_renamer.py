import os

files = []

work_directory = input("Insert dir here: ")

if work_directory == "":
    work_directory = os.path.dirname(os.path.realpath(__file__))

print(work_directory)
os.chdir(work_directory)

firstnum = input("Start number: ")

currentnum = int(firstnum)

this_script = os.path.basename(__file__)
# print("file")
# print(this_script)

for (dirpath, dirnames, filenames) in os.walk(work_directory):
    files.extend(filenames)
    break
    
sorted_files = sorted(files)

try:
    sorted_files.remove(this_script)
    print("removed py file")
except:
    print("Had to put something in this except")
print()

print(f"Firstnum{firstnum}")
print()
test_num = currentnum
for line in sorted_files:
    num_str = (f"{test_num:04d}")
    print(f"{line} -> {num_str}.jpg")
    test_num = test_num + 1

print()

confirm = input(f"Change these files? From {work_directory}? y/N ")


if confirm.lower() == "y":
    #run stuff
    print("Confirmed")
    for file in sorted_files:
        num_str = (f"{currentnum:04d}")
        print(num_str)
        os.rename(file, f"{num_str}.jpg")
        currentnum = currentnum + 1
else:
    #Don't run stuff
    print("Failing")
    
    
print()
print("Done")
